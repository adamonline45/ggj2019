﻿using UnityEngine;

public class BackgroundSkyBehaviour : MonoBehaviour
{

    [SerializeField]
    private float m_YOffset = 0.0f;
    private float YOffset
    {
        get { return m_YOffset; }
        set { m_YOffset = value; }
    }

    private void LateUpdate()
    {
        var camPos = PlayerCamera.instance.cam.transform.position;

        transform.position = new Vector2(camPos.x, camPos.y + YOffset);
    }
}
