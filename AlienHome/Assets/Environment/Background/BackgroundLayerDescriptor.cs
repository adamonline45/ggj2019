﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BackgroundLayerDescriptor : System.Object
{
    [SerializeField]
    private string m_Name = "Layer";
    public string Name
    {
        get { return m_Name; }
    }


    [SerializeField]
    private float m_Distance = 10.0f;
    public float Distance
    {
        get { return m_Distance; }
    }

    [SerializeField]
    private float m_YParallaxModifier = 0.05f;
    public float YParallaxModifier
    {
        get { return m_YParallaxModifier; }
        set { m_YParallaxModifier = value; }
    }


    [SerializeField]
    private float m_YOffset = 0.0f;
    private float YOffset
    {
        get { return m_YOffset; }
        set { m_YOffset = value; }
    }


    [SerializeField]
    private List<BackgroundDecorationDescriptor> m_DecorationDescriptors = null;
    private List<BackgroundDecorationDescriptor> DecorationDescriptors
    {
        get { return m_DecorationDescriptors; }
    }


    [SerializeField]
    private BackgroundLandscapeDescriptor m_LandscapeDescriptor = null;
    private BackgroundLandscapeDescriptor LandscapeDescriptor
    {
        get { return m_LandscapeDescriptor; }
    }


    

    
    public Transform Generate(float width, ref int sortingOrder)
    {
        var layerGo = new GameObject(string.Format("Layer {0}", Name));
        var layer = layerGo.AddComponent<BackgroundLayerBehaviour>();

        layer.transform.Translate(0.0f, YOffset, 0.0f);

        layer.Distance = Distance;
        layer.YRatio = YParallaxModifier;
        layer.YOffset = YOffset;

        var layerRoot = layerGo.transform;

        // Make the decorations
        foreach (var decorationDescriptor in DecorationDescriptors)
        {
            var decorationRoot = decorationDescriptor.Generate( width, ref sortingOrder);

            decorationRoot.SetParent(layerRoot, false);
        }


        // Make the landscape (the ground that 'holds' the decorations)
        var landscapeRoot = LandscapeDescriptor.Generate(width, ref sortingOrder);

        landscapeRoot.SetParent(layerRoot, false);


        return layerRoot;
    }

    
}

