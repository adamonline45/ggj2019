﻿using UnityEngine;

public class LevelBoundsRectBehaviour : MonoBehaviour
{

    [SerializeField]
    private Collider2D m_Collider;
    private Collider2D Collider
    {
        get { return m_Collider; }
        set { m_Collider = value; }
    }


    public Vector2 Center
    {
        get
        {
            return Collider.bounds.center;
        }
    }

    public Vector2 Size {  get { return Collider.bounds.size; } }

    public Vector2 Position {  get { return Collider.offset; } }

    public float Width {  get { return Collider.bounds.size.x; } }
}
