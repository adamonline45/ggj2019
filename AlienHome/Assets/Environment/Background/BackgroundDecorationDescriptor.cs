﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class BackgroundDecorationDescriptor : System.Object
{
    [SerializeField]
    private string m_Name = "Decoration";
    public string Name
    {
        get { return m_Name; }
    }

    [SerializeField]
    private Sprite m_Sprite = null;
    public Sprite Sprite
    {
        get { return m_Sprite; }
    }

    [SerializeField]
    private float m_Spacing = 10.0f;
    public float Spacing
    {
        get { return m_Spacing; }
    }



    [SerializeField]
    private float m_SpacingDeviation = 5.0f;
    public float SpacingDeviation
    {
        get { return m_SpacingDeviation; }
    }

    [SerializeField]
    private float m_BaseVerticalPosition = 2.0f;
    public float VerticalPosition
    {
        get { return m_BaseVerticalPosition; }
    }

    [SerializeField]
    private float m_VerticalDeviation = 2.0f;
    public float VerticalDeviation
    {
        get { return m_VerticalDeviation; }
    }


    public Transform Generate( float width, ref int sortingOrder )
    {

        var decorationRoot = new GameObject(Name).transform;

        // Save these for sorting at the end...
        var decorationRenderers = new List<SpriteRenderer>();


        var currentX = -width / 2.0f;

        while (currentX < width / 2.0f)
        {
            var newGo = new GameObject(String.Format("{0}_{1}", Name, decorationRenderers.Count));
            newGo.transform.SetParent(decorationRoot, true);

            currentX += UnityEngine.Random.Range(-SpacingDeviation, SpacingDeviation);

            var y = VerticalPosition;
            y += UnityEngine.Random.Range(-VerticalDeviation, VerticalDeviation);

            newGo.transform.SetParent(decorationRoot, true);
            newGo.transform.localPosition = new Vector2(currentX, y);

            var spriteRenderer = newGo.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = Sprite;
            spriteRenderer.flipX = UnityEngine.Random.Range(0, 2) == 0;

            decorationRenderers.Add(spriteRenderer);

            currentX += Spacing;
        }

        // Now that we have them all made, apply z-sorting order...
        SetDecorationSortOrder(decorationRenderers, ref sortingOrder);

        return decorationRoot;
    }

    private void SetDecorationSortOrder(IList<SpriteRenderer> decorations, ref int sortingOrder)
    {
        // Arrange sorting order so higher (further) elements are behind lower ones.
        foreach (var d in decorations.OrderBy(d => d.transform.position.y))
            d.sortingOrder = sortingOrder--;
    }

}

