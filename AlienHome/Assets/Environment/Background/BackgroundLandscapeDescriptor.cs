﻿using System;
using UnityEngine;

[Serializable]
public class BackgroundLandscapeDescriptor : System.Object
{
    [SerializeField]
    private Sprite m_Sprite = null;
    private Sprite Sprite
    {
        get { return m_Sprite; }
    }


    [SerializeField]
    private float m_YOffset = 0.0f;
    private float YOffset
    {
        get { return m_YOffset; }
        set { m_YOffset = value; }
    }

    public Transform Generate(float width, ref int sortingOrder)
    {
        var landscapeRoot = new GameObject("Landscape").transform;

        var w = Sprite.bounds.size.x;
        var h = Sprite.bounds.size.y;

        var curX = -width / 2.0f + w/2.0f;

        int landscapeTileIndex = 0;
        while (curX < width / 2.0f)
        {
            var newGo = new GameObject(String.Format("LandscapeTile_{0}", landscapeTileIndex));

            var spriteRenderer = newGo.AddComponent<SpriteRenderer>();

            spriteRenderer.sprite = Sprite;
            spriteRenderer.sortingOrder = sortingOrder--;

            spriteRenderer.transform.SetParent(landscapeRoot, true);
            spriteRenderer.transform.localPosition = new Vector2(curX, YOffset);


            landscapeTileIndex++;
            curX += Sprite.bounds.size.x;
        }

        return landscapeRoot;
    }
}

