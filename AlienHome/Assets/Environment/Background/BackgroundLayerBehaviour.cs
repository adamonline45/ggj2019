﻿using UnityEngine;

public class BackgroundLayerBehaviour : MonoBehaviour
{

    [SerializeField]
    private float m_Distance;
    public float Distance
    {
        get { return m_Distance; }
        set { m_Distance = value; }
    }


    [SerializeField]
    private float m_YRatio = 0.05f;
    public float YRatio
    {
        get { return m_YRatio; }
        set { m_YRatio = value; }
    }



    [SerializeField]
    private float m_YOffset = 0.0f;
    public float YOffset
    {
        get { return m_YOffset; }
        set { m_YOffset = value; }
    }

    private void LateUpdate()
    {
        var bounds = BackgroundBehaviour.LevelBoundsRect;
        var cam = PlayerCamera.instance.cam;
        var camPos = new Vector2(cam.transform.position.x, cam.transform.position.y);
        var camRelativePos = camPos - bounds.Position;
        var normalizedCamPos = new Vector2(camRelativePos.x / bounds.Size.x, camRelativePos.y / bounds.Size.y) * 2.0f;

        //Debug.Log("normalizedCamPos is " + normalizedCamPos);

        var posDelta = new Vector2(normalizedCamPos.x * bounds.Size.x, normalizedCamPos.y * bounds.Size.y * YRatio);
        posDelta = posDelta * Distance;
        //var posDelta = new Vector2(normalizedCamPos.x * Distance, normalizedCamPos.y * Distance * YRatio);
        //var posDelta = new Vector2();
        //Debug.Log(gameObject.name + ": [" + posDelta.x + ", " + posDelta.y + "]");
        var newPos = new Vector2(camPos.x - posDelta.x, camPos.y - posDelta.y + YOffset);

        transform.position = newPos;
    }
}
