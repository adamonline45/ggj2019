﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BackgroundBehaviour : MonoBehaviour
{
    [Header("Component References")]


    [SerializeField]
    private Transform m_LayerAttachmentPoint = null;
    private Transform LayerAttachmentPoint
    {
        get { return m_LayerAttachmentPoint; }
    }

    [SerializeField]
    private LevelBoundsRectBehaviour m_LevelBoundsRect = null;

    private void OnEnable()
    {
        BackgroundBehaviour.LevelBoundsRect = m_LevelBoundsRect;
    }

    private void OnDisable()
    {
        BackgroundBehaviour.LevelBoundsRect = null;
    }

    public static LevelBoundsRectBehaviour LevelBoundsRect { get; private set; }



    [Header("Configuration")]
    
    [SerializeField]
    private int m_StartSortingOrder = -1;
    public int StartSortingOrder
    {
        get { return m_StartSortingOrder; }
    }

    [SerializeField]
    private List<BackgroundLayerDescriptor> m_LayerDescriptors = null;
    private List<BackgroundLayerDescriptor> LayerDescriptors
    {
        get { return m_LayerDescriptors; }
        set { m_LayerDescriptors = value; }
    }



    [HideInInspector]
    [SerializeField]
    private List<GameObject> m_Layers = null;
    private List<GameObject> Layers
    {
        get { return m_Layers; }
        set { m_Layers = value; }
    }



    public void Clear()
    {
        // ToList so we aren't modiying the collection during enumeration.
        foreach (Transform t in LayerAttachmentPoint.GetChildren().ToList() )
        {
            if (Application.isEditor)
                DestroyImmediate(t.gameObject);
            else
                Destroy(t.gameObject);
        }
    }

    public void Generate()
    {
        Clear();


        BackgroundBehaviour.LevelBoundsRect = m_LevelBoundsRect;


        LayerAttachmentPoint.position = LevelBoundsRect.Center;


        int sortingOrder = StartSortingOrder;

        var width = LevelBoundsRect.Width * 1.25f;


        foreach (var layerDescriptor in LayerDescriptors)
        {
            var layerRoot = layerDescriptor.Generate( width, ref sortingOrder);

            LayerAttachmentPoint.AddChild(layerRoot, true);
        }


        Debug.Log("sortingOrder is " + sortingOrder + " (This is how many elements were generated.)");
    }
}

