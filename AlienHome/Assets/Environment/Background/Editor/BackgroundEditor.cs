﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(BackgroundBehaviour))]
public class BackgroundEditor : Editor
{
    private BackgroundBehaviour Target { get { return target as BackgroundBehaviour; } }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Clear"))
        {
            Target.Clear();
            EditorUtility.SetDirty(Target);
        }

        if (GUILayout.Button("Generate"))
        {
            Target.Generate();
            EditorUtility.SetDirty(Target);
        }
    }
}
