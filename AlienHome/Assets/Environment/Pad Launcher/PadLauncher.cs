﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadLauncher : MonoBehaviour
{
    public float launchSpeed;

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Colliding...");
        var bounceable = col.GetComponent<BounceableBehaviour>();

        if (bounceable == null) { 
            print("Did not find bounceable.");
            return;
        }
        bounceable.Rigidbody.AddForceAtPosition(Vector2.up * launchSpeed, bounceable.transform.position, ForceMode2D.Impulse);
    }
}
