﻿using System;
using UnityEngine;

public class BounceableBehaviour : MonoBehaviour
{
    public event Action Bounced = delegate { };

	public void Awake() {
		if(!m_Rigidbody) { m_Rigidbody = GetComponent<Rigidbody2D>(); }
	}

	[SerializeField]
    private Rigidbody2D m_Rigidbody = null;
    public Rigidbody2D Rigidbody
    {
        get { return m_Rigidbody; }
    }

    public void HandleBounced()
    {
        Bounced();
    }
}
