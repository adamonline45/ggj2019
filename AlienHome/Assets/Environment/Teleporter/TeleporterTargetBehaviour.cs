﻿using UnityEngine;

public class TeleporterTargetBehaviour : MonoBehaviour
{
    [Header("Component References")]

    [SerializeField]
    private Transform m_SpawnPoint = null;
    public Transform SpawnPoint
    {
        get { return m_SpawnPoint; }
    }
    
    [SerializeField]
    private ParticleSystem m_ParticleSystem = null;
    private ParticleSystem ParticleSystem
    {
        get
        {
            return m_ParticleSystem;
        }
    }


    public void ReceiveTeleportable(TeleportableBehaviour teleportable)
    {
        teleportable.RootTransform.position = SpawnPoint.position;

        ParticleSystem.Play();

        teleportable.HandleTeleported();
    }
}
