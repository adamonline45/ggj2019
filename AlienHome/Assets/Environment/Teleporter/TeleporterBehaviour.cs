﻿using UnityEngine;

public class TeleporterBehaviour : MonoBehaviour
{

    [Header("Component References")]

    [SerializeField]
    private ParticleSystem m_ParticleSystem = null;
    private ParticleSystem ParticleSystem
    {
        get
        {
            return m_ParticleSystem;
        }
    }

   [Header("Project References")]

    [SerializeField]
    private AudioClip m_TeleportingAudioClip = null;
    private AudioClip TeleportingAudioClip
    {
        get { return m_TeleportingAudioClip; }
        set { m_TeleportingAudioClip = value; }
    }

    [Header("Scene References")]

    [SerializeField]
    private TeleporterTargetBehaviour m_Target = null;
    private TeleporterTargetBehaviour Target
    {
        get { return m_Target; }
        set { m_Target = value; }
    }


    // Lazy init
    private AudioSource m_AudioSource;
    private AudioSource AudioSource
    {
        get
        {
            return m_AudioSource ?? (m_AudioSource = GetComponent<AudioSource>());
        }
    }


    private void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Teleporter entered!");

        var teleportable = collider.GetComponent<TeleportableBehaviour>();

        if (teleportable == null)
            return;

        AudioSource.PlayOneShot(TeleportingAudioClip);

        ParticleSystem.Play();

        Target.ReceiveTeleportable(teleportable);
    }
}
