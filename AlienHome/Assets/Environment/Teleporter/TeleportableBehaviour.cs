﻿using System;
using UnityEngine;

public class TeleportableBehaviour : MonoBehaviour
{
    public event Action Teleported = delegate { };

    [SerializeField]
    private Transform m_RootTransform = null;

	public void Awake() {
		if(m_RootTransform == null) m_RootTransform = transform;
	}

	public Transform RootTransform
    {
        get { return m_RootTransform; }
    }

    public void HandleTeleported()
    {
        Teleported();
    }
}
