# Teleporter

## Purpose
Allows an entity to teleport from one part of the world to another.

## How to use:

1. Add a TeleporterPrefab to the scene.
1. Add a TeleporterTargetPrefab to the scene.
1. In the inspector, bind the TeleporterPrefab's Target property to the TeleporterTargetPrefab.
1. Add a TeleportableBehaviour component to any entities you wish to utilize teleporters.
    * The teleportable entity's other components can optionally bind to the Teleportable component's Teleported event, which is invoked after teleportation occurs.

## More Details:
**TeleporterPrefab**

* The prefab that initiates the teleportation action when an entity enters it.
* In order for an entity to be teleported, it must have a TeleportableBehaviour as a component.
* The teleported entity will be teleported to the specified TeleporterTarget.

**TeleporterTargetPrefab**

* The prefab that a teleportable entity will teleport to.

**TeleportableBehaviour**

* This component must be added to any entity that can use the teleporter.
* Provides a Teleported event that an entity's other components can bind to in order to provide entity-specific behaviour in response to a teleportation.

## Future thoughts:

* Make teleportation not be intantaneous, animating or fading the teleported entity.
