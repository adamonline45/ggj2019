﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class TransformExtensions
{
    public static IEnumerable<Transform> GetChildren(this Transform transform)
    {
        return transform.Cast<Transform>();
    }

    public static void AddChild(this Transform parent, Transform child)
    {
        child.SetParent(parent);
    }

    public static void AddChild(this Transform parent, Transform child, bool worldPositionStays)
    {
        child.SetParent(parent, worldPositionStays);
    }
}
