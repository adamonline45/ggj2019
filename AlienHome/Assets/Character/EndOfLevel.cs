﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndOfLevel : MonoBehaviour
{
	[Tooltip("Don't forget to add the levels to the Build Settings")]
	public string nextSceneName;

	public void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Player")
        {
            SceneManager.LoadScene(nextSceneName);
        }
	}
}
