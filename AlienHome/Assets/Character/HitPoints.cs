﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPoints : MonoBehaviour
{
	public float hitPoints = 10;
	public SpriteRenderer spriteRenderer;
	public float damageCooldownTimer = 1;
	float timer;

    void Start() {}

	public bool IsAlive { get => hitPoints > 0; }

	void Update() {
		if(timer < damageCooldownTimer) {
			timer += Time.deltaTime;
			if(timer >= damageCooldownTimer && spriteRenderer != null) {
				spriteRenderer.enabled = true;
			}
		}
		if(!CanTakeDamage() && spriteRenderer != null) {
			bool hide = (int)(timer * 10) % 2 == 0;
			spriteRenderer.enabled = hide;
		}
	}

	public bool CanTakeDamage() { return timer >= damageCooldownTimer; }

	public void TakeDamage(Damage source) {
		if(CanTakeDamage() && IsAlive) {
			hitPoints -= source.damage;
			PlayerCamera.instance.EmitParticle(transform.position, Color.red, (int)(source.damage * 10));

			timer = 0;
			PlayerController pc = GetComponent<PlayerController>();
			if(pc.canInput) {
				PlayerCamera.instance.UpdateHPText();
			}
			if(hitPoints <= 0) {
				AgentAnimator aa = GetComponent<AgentAnimator>();
				aa.Collapse();
				pc.canInput = false;
				AIBehaviour ai = GetComponent<AIBehaviour>();
				ai.enabled = false;
			}
		}
	}
}
