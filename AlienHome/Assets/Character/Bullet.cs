﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
	public float maxLifetime = 30;
	float timer;
	public bool destroyOnCollision;

    void Start() {}

    void Update() {
		timer += Time.deltaTime;
		if(timer > maxLifetime) {
			Destroy(gameObject);
		}
	}

	public void Hit(GameObject other) {
		Damage dmg = GetComponent<Damage>();
		if(dmg != null) {
			dmg.DoTheDamage(other.GetComponent<HitPoints>());
		}
		if(destroyOnCollision) { Destroy(gameObject); }
	}

	//public void OnTriggerStay2D(Collider2D collision) {
	//	Hit(collision.gameObject);
	//}
	//public void OnTriggerEnter2D(Collider2D collision) {
	//	Hit(collision.gameObject);
	//}
	//public void OnTriggerExit2D(Collider2D collision) {
	//	Hit(collision.gameObject);
	//}
	public void OnCollisionEnter2D(Collision2D collision) {
		Hit(collision.gameObject);
	}
	private void OnCollisionStay2D(Collision2D collision) {
		Hit(collision.gameObject);
	}
	private void OnCollisionExit2D(Collision2D collision) {
		Hit(collision.gameObject);
	}
}
