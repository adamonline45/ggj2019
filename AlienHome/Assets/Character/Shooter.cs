﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
	public GameObject bullet;
	public float bulletSpeed = 10;
	public Transform ejectionPoint;
	public float shootCooldown = 1;
	float timer; 
	long lastShot;

    void Start(){}

	public static long NowRealtime() { return System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond; }
	public Transform root;

	public void Shoot() {
		long now = NowRealtime();
		timer = (now - lastShot) / 1000f;
		if(timer >= shootCooldown) {
			timer = 0;
			CreateBullet();
			lastShot = now;
		}
	}
	public GameObject CreateBullet() {
		GameObject go = Instantiate(bullet, ejectionPoint.position, ejectionPoint.rotation);
		Rigidbody2D rb = go.GetComponent<Rigidbody2D>();
		if(rb != null) {
			rb.velocity = rb.transform.right * bulletSpeed;
		} else {
			Debug.Log("no rb?");
		}
		return go;
	}

	private void Awake() { Shoot(); }

	void Update() { Shoot(); }
}
