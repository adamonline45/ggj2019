﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCamera : MonoBehaviour
{
	public Camera cam;
	public static PlayerCamera instance;
	public TMPro.TMP_Text top, bottom;
	public ParticleSystem playerIndicator;
	PlayerController pc;
	HitPoints pchp;
	AgentAnimator aa;
	public float minimumY = -50;
	public float forcedRespawnAfterDeathTimer = 6;
	float respawnTimer = -1;
	[HideInInspector]
	public float resistTimer = 0;
	[HideInInspector]
	public bool resisting = false;
	[HideInInspector]
	public PlayerController.InputEvent resistEvent;
	public GameObject createWhenPIsPressed;
	public PlayerController GetPlayer() {
		Cinemachine.CinemachineVirtualCamera vcam = GetComponent<Cinemachine.CinemachineVirtualCamera>();
		if(vcam != null && vcam.m_Follow != null) {
			return vcam.m_Follow.GetComponent<PlayerController>();
		}
		return null;
	}
	public void SetPlayer(PlayerController a_pc) {
		if(pc != null) {
			pc.canInput = false;
			if(pchp != null) {
				pc.GetComponent<AIBehaviour>().enabled = pchp.hitPoints > 0;
			}
			pc.jumpPressed = false;
		}
		Cinemachine.CinemachineVirtualCamera vcam = GetComponent<Cinemachine.CinemachineVirtualCamera>();
		this.pc = a_pc;
		if(pc != null) {
			vcam.m_Follow = pc.transform;
			playerIndicator.transform.SetParent(pc.transform);
			playerIndicator.transform.localPosition = Vector3.zero;
			pc.canInput = true;
			pc.GetComponent<AIBehaviour>().enabled = true;
		} else {
			vcam.m_Follow = null;
		}
		top.text = "inhabiting a " + pc.category+"\n"+pc.helptext;
		UpdateHPText();
		this.pchp = this.pc.GetComponent<HitPoints>();
		this.aa = this.pc.GetComponent<AgentAnimator>();
		if(pchp.hitPoints > 0) {
			respawnTimer = forcedRespawnAfterDeathTimer;
		}
	}
	public void EmitParticle(Vector3 position, Color c, int count) {
		ParticleSystem.EmitParams em = new ParticleSystem.EmitParams();
		em.position = position;
		em.startColor = c;
		playerIndicator.Emit(em, count);
	}
	public void UpdateHPText() {
		HitPoints hp = GetPlayer().GetComponent<HitPoints>();
		bottom.text = "hitpoints: " + hp.hitPoints;
	}
	void Start()
    {
        if(instance != null) {
			throw new System.Exception("NOT SUPPOSED TO HAVE MORE THAN ONE");
		}
		instance = this;
	}
	public void RestartLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	public void NextLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
	}
	private void Update() {
		if(pc != null) {
			if(pc.transform.position.y < minimumY || Input.GetKeyDown(KeyCode.Escape)) {
				RestartLevel();
			}
			if(Input.GetKeyDown(KeyCode.N)) {
				NextLevel();
			}
			if(pchp.hitPoints <= 0) {
				top.text = "inhabit someone new...\n" + ((int)respawnTimer);
				respawnTimer -= Time.deltaTime;
				if(respawnTimer <= 0) {
					RestartLevel();
				}
			}
			aa.UpdateResist(this);
		}
		if(createWhenPIsPressed != null && Input.GetKeyDown(KeyCode.P)) {
			GameObject go = Instantiate(createWhenPIsPressed, transform.position, Quaternion.identity);
			Vector3 v = go.transform.position;
			v.z = 0;
			go.transform.position = v;
		}
	}
}
