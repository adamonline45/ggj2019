﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public System.Action walk, jump, land, attack, action, flip, collapse, arise, hit;
	public string category = "unique agent type name", helptext = "left/right/jump/fire";

	//Movement Variables
	public int playerSpeed; //player walk speed
	[HideInInspector]
	public bool facingRight = false; //variable to determine player direction
	public int jumpPower; //player jump height
	public float moveX; //direction player is facing
	public bool jumpPressed;
	public bool attackPressed;
	public bool canInput; //variable to determine if player can press any buttons
	public float topFallSpeed; //terminal fall velocity

	//jumping variables
	public bool grounded = false; //variable to determine player is on the ground
	public bool canAlwaysJump = false;
	public bool canJump; //variable to determine if the player can jump
	float groundCheckRadius = 0.2f; //radius of ground check area
	public LayerMask groundLayer; //layer the ground is on
	public Transform groundCheck; //point where grounded is checked

	public Vector2 counterForceFromWall;

	public void FlipPlayer() {
		facingRight = !facingRight;
		if(flip != null) { flip.Invoke(); }
	}

	[HideInInspector]
	public Rigidbody2D rb;

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	public enum InputEvent { none, moveLeft, moveRight, jump, attack }

	public void DoEvent(InputEvent e) {
		switch(e) {
		case InputEvent.moveLeft: moveX = -1; break;
		case InputEvent.moveRight: moveX = 1; break;
		case InputEvent.jump: jumpPressed = true; break;
		case InputEvent.attack: attackPressed = true; break;
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (canInput) {
			moveX = Input.GetAxis("Horizontal");
			jumpPressed = Input.GetButton("Jump");
			attackPressed = Input.GetButton("Fire1");
			if(Input.GetKeyDown(KeyCode.X)) { if(collapse != null) { collapse.Invoke(); } }
			if(Input.GetKeyDown(KeyCode.Z)) { if(arise != null) { arise.Invoke(); } }
		}
		if(attackPressed) { if(attack != null) { attack.Invoke(); } attackPressed = false; }
		PlayerMove();
		if(!canInput) {
			moveX = 0;
		}
	}

	private void FixedUpdate()
	{
		if(canInput && PlayerCamera.instance.GetPlayer() != this) {
			canInput = false;
		}

		bool wasOnGround = grounded;
		//Checks if player on ground, if not then player is falling
		grounded = canAlwaysJump || onAJumpable || Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);

		//if(!grounded && rb.velocity.y < 0) {
		//	BoxCollider2D box = GetComponent<BoxCollider2D>();
		//	Vector2 p = (Vector2)transform.position + (Vector2.down*box.size.y/2);
		//	RaycastHit2D rh = Physics2D.Raycast(p, Vector2.down, 0.125f);
		//	if(rh.collider != null && rh.distance < 0.125f && rh.collider.GetComponent<Jumpable>()) {
		//		grounded = true;
		//	}
		//}

		if(grounded && !wasOnGround && rb.velocity.y < 0) {
			if(land != null) { land.Invoke(); }
		}

		if (rb.velocity.y < -topFallSpeed)
		{
			rb.velocity = new Vector2(rb.velocity.x, -topFallSpeed);
		}
		onAJumpable = false;
		counterForceFromWall = Vector2.zero;
	}

	void PlayerMove()
	{
		//CONTROLS
		if (jumpPressed && grounded) //if player is on the ground and has not jumped yet
		{
			grounded = false;
			Jump();
			if(jump != null) { jump.Invoke(); }
		} else if (!jumpPressed && rb.velocity.y > 0) //released before apex of the jump
		{
		    rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * .3f);
		}


		//DIRECTION
		if (moveX < 0.0f && !facingRight) { FlipPlayer(); }//flips player left
		else if (moveX > 0.0f && facingRight) { FlipPlayer(); } //flips player right

		if(moveX != 0) {
			moveX += counterForceFromWall.x; // PHYSICS - counter-balance wall normal
			if(walk != null) { walk.Invoke(); }
		}

		//PHYSICS
		rb.velocity = new Vector2( moveX * playerSpeed, rb.velocity.y);

    }

    void Jump() //Jump Code
    {
        rb.AddForce(Vector2.up * jumpPower);
    }
	bool onAJumpable = false;
	public void OnCollisionStay2D(Collision2D collision) {
		int groundLayer = LayerMask.NameToLayer("Ground");
		if(collision.collider.gameObject.layer == groundLayer || (onAJumpable = collision.collider.GetComponent<Jumpable>() != null)) {
			Vector2 v = collision.contacts[0].normal;
			if(v.x != 0) { // ignore ground or ceiling counterforces
				counterForceFromWall = v;
			}
		}
	}

}
