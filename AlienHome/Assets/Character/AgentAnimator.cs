﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO trampoline consistency

public class AgentAnimator : MonoBehaviour
{
	public SpriteRenderer spriteRenderer;

	public enum RotationAnimationType { None, Waddle, LeanSprite, LeanTransform, Arise}

	public PlayerController.InputEvent[] rejectionMoves = { 
		PlayerController.InputEvent.none,
		PlayerController.InputEvent.none,
		PlayerController.InputEvent.none,
		PlayerController.InputEvent.none,
		PlayerController.InputEvent.none,
		PlayerController.InputEvent.none,
		PlayerController.InputEvent.moveLeft,
		PlayerController.InputEvent.moveLeft,
		PlayerController.InputEvent.moveLeft,
		PlayerController.InputEvent.moveRight,
		PlayerController.InputEvent.moveRight,
		PlayerController.InputEvent.moveRight,
		PlayerController.InputEvent.jump,
		PlayerController.InputEvent.jump,
		PlayerController.InputEvent.jump,
		PlayerController.InputEvent.attack,
		PlayerController.InputEvent.attack,
		};
	public float acceptDurationWiggleMultiplier = 2;
	public float acceptDuration = 5;
	public float acceptDurationRate = 0.95f;
	public float resistDurationWiggleMultiplier = 1.5f;
	public float resistDuration = .05f;
	public float resistDurationRate = 1.1f;

	public void IncrementResistStats(float deltaTime) {
		acceptDuration *= acceptDurationRate;
		resistDuration *= resistDurationRate;
	}

	public void UpdateResist(PlayerCamera pcam) {
		pcam.resistTimer -= Time.deltaTime;
		if(pcam.resistTimer <= 0) {
			if(pcam.resisting) {
				pcam.resisting = false;
				pc.canInput = GetComponent<HitPoints>().hitPoints > 0;
				pcam.resistTimer = acceptDuration * Random.Range(1f, acceptDurationWiggleMultiplier);
				IncrementResistStats(1);
			} else {
				pcam.resisting = true;
				pc.canInput = false;
				float duration = resistDuration * Random.Range(1f, acceptDurationWiggleMultiplier);
				//ParticleSystem.EmitParams em = new ParticleSystem.EmitParams();
				//em.startColor = Color.magenta;
				//pcam.playerIndicator.Emit(em, (int)(duration * 50));
				pcam.EmitParticle(transform.position, Color.magenta, (int)(duration * 50));
				pcam.resistTimer = duration;
				int i = (int)(Random.value * rejectionMoves.Length);
				if(i < rejectionMoves.Length) {
					pcam.resistEvent = rejectionMoves[i];
				}
				string randoChar = "!@#$%^&*()_+{}:\"<>?,./;\'[]-=";
				pcam.top.text = ReplaceRandomNonNewlineWithSpace(pcam.top.text,
					randoChar[(int)(Random.value * randoChar.Length)]);
				// = "inhabiting a " + pc.category + "\n" + pc.helptext;

			}
		}
		if(pcam.resisting) {
			pc.DoEvent(pcam.resistEvent);
		}
	}

	public string ReplaceRandomNonNewlineWithSpace(string s, char c = ' ') {
		string t = s;
		int i, iter = 0;
		do {
			i = (int)(Random.value * (t.Length - 1));
		} while(s[i] == '\n' && iter++ < 1000);
		t = s.Substring(0, i) + (c!='\0'?c.ToString():"") + s.Substring(i + 1);
		return t;
	}

	[System.Serializable]
	public class SpriteAnimation {
		public string name;
		public float delay;
		public Sprite[] sprites;
		public RotationAnimationType rotationAnimation;
		public float angle, anglePerSecond;
		public Transform enableDuringAnimation;
		[HideInInspector]
		public RotationAnimation_None rotanim;
		public AudioClip noise;

		public RotationAnimation_None GetRotationAnimator() {
			if(rotanim == null) {
				string rotAnimType = typeof(AgentAnimator) + "+RotationAnimation_" + rotationAnimation;
				rotanim = AIBehaviour.CreateNew(System.Type.GetType(rotAnimType)) as RotationAnimation_None;
			}
			return rotanim;
		}
	}
	public SpriteAnimation[] sprites = { 
		new SpriteAnimation{name="idle",sprites=new Sprite[]{null},delay=.2f,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="walk",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.Waddle, angle=90,anglePerSecond=180},
		new SpriteAnimation{name="jump",sprites=new Sprite[]{null},delay=.2f,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="attack",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.LeanSprite,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="activate",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.LeanSprite,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="collapse",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.LeanTransform,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="arise",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.Arise,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="land",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.None,angle=90,anglePerSecond=180},
		new SpriteAnimation{name="hit",sprites=new Sprite[]{null},delay=.2f,rotationAnimation=RotationAnimationType.LeanSprite,angle=-10,anglePerSecond=360},
	};

	public string GetAnimationName() { return current.name; }

	private SpriteAnimation current;
	private RotationAnimation_None rotationAnimator;
	PlayerController pc;

	private float animationTimer;
	private int animationIndex;

	private float codeAnimDurationLeft = 0;
	private float codeAnimTimer;

	public SpriteAnimation GetAnimation(string name) {
		return System.Array.Find(sprites, sa => sa.name == name);
	}

	public void SetCurrentAnimationListTo(string name) {
		if(rotationAnimator != null) { rotationAnimator.Exit(this); }
		if(current != null && current.enableDuringAnimation != null) {
			current.enableDuringAnimation.gameObject.SetActive(false);
		}
		current = GetAnimation(name);
		if(current == null) { Debug.Log("WHERE IS " + name + " FOR " + this); }
		rotationAnimator = current.GetRotationAnimator();
		if(current.enableDuringAnimation != null) {
			current.enableDuringAnimation.gameObject.SetActive(true);
		}
		if(rotationAnimator != null) { rotationAnimator.Enter(this); }
		if(current.noise != null) {
			NS.Noisy.PlaySound(current.noise, transform.position);
		}
	}

	public class RotationAnimation_None {
		public virtual void Enter(AgentAnimator a) { }
		public virtual void Execute(AgentAnimator a) {
			a.spriteRenderer.transform.localRotation = Quaternion.identity;
		}
		public virtual void Exit(AgentAnimator a) { }
	}
	public class RotationAnimation_Waddle : RotationAnimation_None {
		public override void Execute(AgentAnimator a) {
			float t = a.codeAnimTimer * a.current.anglePerSecond;
			float angle = (int)t % (a.current.angle * 2);
			if(angle > a.current.angle) {
				angle = a.current.angle - (angle - a.current.angle);
			}
			angle -= a.current.angle / 2;
			a.spriteRenderer.transform.localRotation = Quaternion.Euler(0, 0, angle);
		}
	}
	public class RotationAnimation_LeanSprite : RotationAnimation_None {
		public override void Enter(AgentAnimator a) {
			a.codeAnimDurationLeft = a.current.angle * 2 / a.current.anglePerSecond;
			a.codeAnimTimer = a.animationTimer = 0;
		}
		public override void Execute(AgentAnimator a) {
			float t = a.codeAnimTimer * a.current.anglePerSecond;
			float angle = (int)t % (a.current.angle * 2);
			if(angle > a.current.angle) {
				angle = a.current.angle - (angle - a.current.angle);
			}
			angle *= -1;
			a.spriteRenderer.transform.localRotation = Quaternion.Euler(0, 0, angle);
		}
	}
	public class RotationAnimation_LeanTransform : RotationAnimation_None {
		public override void Enter(AgentAnimator a) {
			a.codeAnimDurationLeft = 30; //a.current.angle / a.current.anglePerSecond; // TODO math
			a.codeAnimTimer = a.animationTimer = 0;
		}
		public override void Execute(AgentAnimator a) {
			float angle = a.codeAnimTimer * a.current.anglePerSecond;
			if(angle > a.current.angle) { angle = a.current.angle; }
			a.transform.localRotation = Quaternion.Euler(0, 0, angle);
		}
		public override void Exit(AgentAnimator a) {
			a.transform.localRotation = Quaternion.Euler(0, 0, a.current.angle);
		}
	}
	public class RotationAnimation_Arise : RotationAnimation_None {
		public override void Enter(AgentAnimator a) {
			a.codeAnimDurationLeft = 30; //360 / a.current.anglePerSecond; // TODO math
			a.codeAnimTimer = a.animationTimer = 0;
		}
		public override void Execute(AgentAnimator a) {
			Quaternion target = Quaternion.identity;
			if(a.pc.facingRight) {
				target = Quaternion.AngleAxis(180, Vector3.up);
			}
			if(a.transform.localRotation == target) {
				a.SetCurrentAnimationListTo("idle");
				a.transform.localRotation = target;
				//Debug.Log("done?");
			} else {
				a.transform.localRotation = Quaternion.RotateTowards(
				a.transform.localRotation, target,
				a.current.anglePerSecond * Time.deltaTime);
			}
		}
		//public override void Exit(AgentAnimator a) { Debug.Log("I got quit?"); }
	}

	void Awake() {
		current = sprites[0];
		if(spriteRenderer == null) { spriteRenderer = GetComponent<SpriteRenderer>(); }
		System.Array.ForEach(sprites, (s) => {
			if(s.enableDuringAnimation != null) {
				s.enableDuringAnimation.gameObject.SetActive(false);
			}
		});
		SetCurrentAnimationListTo("idle");
		animationIndex = 0;
		spriteRenderer.sprite = current.sprites[animationIndex];
		pc = GetComponent<PlayerController>();
		pc.walk = () => {
			if(current.name != "attack") {
				SetCurrentAnimationListTo("walk");
				codeAnimDurationLeft = 1 / 128f;
			}
		};
		pc.jump = () => {
			SetCurrentAnimationListTo("jump");
			codeAnimDurationLeft = 1f;
		};
		pc.land = () => {
			if(current == null || current.name == "jump" || current.name == "idle" || current.name == "walk") {
				SetCurrentAnimationListTo("land");
			}
		};
		pc.hit = Hit;
		pc.collapse = Collapse;
		pc.arise = Arise;
		pc.attack = Attack;
		pc.flip = () => {
			gameObject.transform.Rotate(0, 180, 0);
		};
	}
	public void Hit() { SetCurrentAnimationListTo("hit"); }
	public void Collapse() { if(current.name != "collapse") { SetCurrentAnimationListTo("collapse"); } }
	public void Arise() { if(current.name != "arise") { SetCurrentAnimationListTo("arise"); } }
	public void Attack() { if(current.name != "attack") { SetCurrentAnimationListTo("attack"); } }
	void Update() {
		animationTimer += Time.deltaTime;
		codeAnimTimer += Time.deltaTime;
		if(animationTimer >= current.delay) {
			animationTimer -= current.delay;
			animationIndex++;
			if(animationIndex >= current.sprites.Length) { animationIndex = 0; }
			spriteRenderer.sprite = current.sprites[animationIndex];
		}
		if(rotationAnimator != null) { rotationAnimator.Execute(this); }
		codeAnimDurationLeft -= Time.deltaTime;
		if(codeAnimDurationLeft <= 0 && current.name != "idle") {
			SetCurrentAnimationListTo("idle");
		}
	}

	public void OnMouseDown() {
		PlayerCamera.instance.SetPlayer(pc);
	}
}
