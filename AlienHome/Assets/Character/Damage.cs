﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
	public float damage;

	public void Start() {
		//GetComponent<BoxCollider2D>().isTrigger = true;
	}
	public void DoTheDamage(HitPoints hp) {
		if(hp != null) {
			hp.TakeDamage(this);
		}
	}

	public void OnTriggerStay2D(Collider2D collision) {
		DoTheDamage(collision.gameObject.GetComponent<HitPoints>());
	}
	public void OnTriggerEnter2D(Collider2D collision) {
		DoTheDamage(collision.gameObject.GetComponent<HitPoints>());
	}
	public void OnTriggerExit2D(Collider2D collision) {
		DoTheDamage(collision.gameObject.GetComponent<HitPoints>());
	}
	public void OnCollisionEnter2D(Collision2D collision) {
		DoTheDamage(collision.gameObject.GetComponent<HitPoints>());
	}
	private void OnCollisionStay2D(Collision2D collision) {
		DoTheDamage(collision.gameObject.GetComponent<HitPoints>());
	}
	private void OnCollisionExit2D(Collision2D collision) {
		DoTheDamage(collision.gameObject.GetComponent<HitPoints>());
	}
}
