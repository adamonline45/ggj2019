﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(PlayerController))]
public class AIBehaviour : MonoBehaviour
{
	public class Brain_Idle {
		public virtual void Enter(AIBehaviour a) { }
		public virtual void Execute(AIBehaviour a) { }
		public virtual void Exit(AIBehaviour a) { }
	}
	public class Brain_Patrol : Brain_Idle {
		public bool right;
		public override void Execute(AIBehaviour a) {
			// let attacks finish
			if(a.aa.GetAnimationName() == "idle"
			|| a.aa.GetAnimationName() == "walk"
			|| a.aa.GetAnimationName() == "jump") {
				BoxCollider2D box = a.GetComponent<BoxCollider2D>();
				Vector2 forward = Vector2.right;
				if(a.pc.facingRight) forward *= -1;
				Vector2 offset = forward * (box.size.x / 2);
				Vector2 nextPosition = (Vector2)a.pc.transform.position + offset;
				RaycastHit2D hit2d = Physics2D.Raycast(nextPosition, Vector2.down, 1, 1 << LayerMask.NameToLayer("Ground"));
				bool canStandInfront = hit2d.distance > 0 && hit2d.distance < float.PositiveInfinity;
				bool canWalkForward = true;
				if(canStandInfront) {
					hit2d = Physics2D.Raycast(nextPosition, forward, Time.deltaTime * 2, 1 << LayerMask.NameToLayer("Ground"));
					canWalkForward = hit2d.distance <= 0 || hit2d.distance > float.PositiveInfinity;
				}
				//float d = 1; // debug draw the raycasts
				//if(canStandInfront) {
				//	d = hit2d.distance;
				//}
				//NS.Lines.MakeArrow(ref line_down, nextPosition, nextPosition + Vector2.down * d, Color.magenta);
				if(!canStandInfront || !canWalkForward) {
					right = !right;
				}
				a.pc.moveX = right ? 1 : -1;

				if(a.hostileCategories.Length > 0) {
					float range = a.expectedAattackRange;
					Vector3 p = nextPosition + forward * 0.125f;
					hit2d = Physics2D.Raycast(p, forward, range);
					//bool isAttacking = false;
					if(hit2d.collider != null) {
						PlayerController otherPC = hit2d.collider.gameObject.GetComponent<PlayerController>();
						if(otherPC != null
						&& System.Array.IndexOf(a.hostileCategories, otherPC.category) >= 0
						&& otherPC.GetComponent<HitPoints>().IsAlive) {
							a.pc.attack();
							// because activating triggers don't trigger OnTriggerEnter2D, more extreme measures need to be taken
							AgentAnimator.SpriteAnimation sa = a.aa.GetAnimation("attack");
							BoxCollider2D dmger = null;
							if(sa.enableDuringAnimation != null) {
								dmger = sa.enableDuringAnimation.GetComponent<BoxCollider2D>();
							}
							if(dmger) {
								Transform originalParent = dmger.transform.parent;
								dmger.transform.SetParent(null);
								dmger.transform.SetParent(originalParent);
							}
							//isAttacking = true;
							a.pc.moveX = 0;						
						}
					}
					//NS.Lines.MakeArrow(ref line_attack, p, p+(Vector3)forward, isAttacking ? Color.red : Color.yellow);
				}
			} else {
				a.pc.moveX = 0;
			}
		}
		//GameObject line_down, line_attack;
	}

	public enum BrainType { Brain_Idle, Brain_Patrol }

	public BrainType brainType = BrainType.Brain_Idle;

	PlayerController pc;
	AgentAnimator aa;
	public float expectedAattackRange = 1;
	Brain_Idle current;

	public string[] hostileCategories = { };

	void Start()
    {
		pc = GetComponent<PlayerController>();
		aa = GetComponent<AgentAnimator>();
		SetBrain(brainType);
    }

	public void SetBrain(BrainType bt) {
		string typename = typeof(AIBehaviour)+"+" + bt;
		System.Type t = System.Type.GetType(typename);
		Brain_Idle brain = CreateNew(t) as Brain_Idle;
		SetBrain(brain);
	}

	public void SetBrain(Brain_Idle brain) {
		if(current != null) { current.Exit(this); }
		current = brain;
		if(current != null) { current.Enter(this); }
	}

	void Update()
    {
        if(!pc.canInput && current != null) {
			current.Execute(this);
		}
	}


	public static object CreateNew(System.Type t) {
		if(t == null) return null;
		// create arrays as zero-length arrays. hopefully they will be resized/replaced soon enough.
		if(t.IsArray) { return System.Activator.CreateInstance(t, new object[] { 0 }) as System.Array; }
		// for other objects, look for default constructors
		object o = null;
		System.Reflection.ConstructorInfo[] constructors = t.GetConstructors();
		System.Reflection.ConstructorInfo defaultConstructor = null;
		for(int i = 0; i < constructors.Length; ++i) {
			if(constructors[i].GetParameters().Length == 0) {
				defaultConstructor = constructors[i];
				break;
			}
		}
		if(defaultConstructor != null) {
			o = defaultConstructor.Invoke(new object[] { });
		} else if(t.IsValueType) {
			o = System.Activator.CreateInstance(t);
		} else {
			for(int i = 0; i < constructors.Length; ++i) {
				Debug.Log(constructors[i].Name + " " + constructors[i].GetParameters().Length);
			}
			Debug.Log("could not create a new <" + t + ">" + (t.IsValueType ? " (value type)" : "."));
		}
		return o;
	}
}
